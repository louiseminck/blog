<?php

use Cocur\Slugify\Slugify;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

//Request::setTrustedProxies(array('127.0.0.1'));
$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
});

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig', array());
})
->bind('homepage')
;

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

//-------------------------Récupérer les articles-------------------------//

$app->get('/blog', function () use ($app) {
    $sql = "SELECT * FROM articles";
    $posts = $app['db']->fetchAll($sql);

    return $app['twig']->render('blog.html.twig', array(
        'posts' => $posts
    ));

});

$app->get('/blog/{slug}', function ($slug) use ($app) {

    $sql = "SELECT * FROM articles WHERE slug = ?";
    $post = $app['db']->fetchAssoc($sql, array($slug));

    return $app['twig']->render('single.html.twig', array(
        'post' => $post
    ));
    
});

//-----------------------A P I-----------------------//

$app->get('/api/datasingle/{slug}', function ($slug) use ($app) {

    $sql = "SELECT * FROM articles WHERE slug = ?";
    $post = $app['db']->fetchAssoc($sql, array($slug));

    return $app->json($post, 200);
    
});

//-----------------------Modifier un article-----------------------//

$app->get('/admin/edit/{slug}', function ($slug) use ($app) {

    return $app['twig']->render('edit.html.twig', array(
        'slug' => $slug
    ));
});

$app->post('/admin/edit/{slug}', function ($slug, Request $request) use ($app) {

    $newslug = $app['slugify']->slugify($request->request->get('titre'));

    $sql = "UPDATE articles SET titre = :titre, auteur = :auteur, contenu = :contenu, slug = :newslug WHERE slug = :slug";

    $app['db']->executeUpdate($sql, array(
        'titre' => $request->request->get('titre'), //Récupérer les données envoyées du form
        'auteur' => $request->request->get('auteur'), //Récupérer les données envoyées du form
        'contenu' => $request->request->get('contenu'), //Récupérer les données envoyées du form
        'slug' => $slug,
        'newslug' => $newslug
    ));
    
    return $app->json(["message" => "Article bien modifié !"], 200);
    
});

//-----------------------Supprimer un article-----------------------//

$app->post('/admin/delete/{slug}', function ($slug, Request $request) use ($app) {

    $app['db']->delete('articles', array('slug' => $slug));
    return "Bien supprimé";
});

//-----------------------Ajouter un article-----------------------//

$app->get('/admin/insert', function () use ($app) {

    $post = $app['db'];

    return $app['twig']->render('insert.html.twig', array(
        'post' => $post
    ));
    
});

$app->post('/admin/insert', function (Request $request) use ($app) {

    $newslug = $app['slugify']->slugify($request->request->get('titre'));

    $app['db']->insert('articles', array(
        'titre' => $request->request->get('titre'), //Récupérer les données envoyées du form
        'auteur' => $request->request->get('auteur'), //Récupérer les données envoyées du form
        'date' => date("y-m-d"),
        'contenu' => $request->request->get('contenu'), //Récupérer les données envoyées du form
        'slug' => $newslug
    ));

    return "article bien ajouté";
    
});
