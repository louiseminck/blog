<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

$app = new Application();

//-------------------------Connexion avec la Base de données-------------------------//

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'path'     => __DIR__.'/app.db',
        'dbname' => 'dbname',
        'password' => 'password',
        'charset' => 'UTF8',
        'driver' => 'pdo_mysql'
    ),
));

$app->register(new Cocur\Slugify\Bridge\Silex2\SlugifyServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...

    return $twig;
});

return $app;
