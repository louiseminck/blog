# Documentation 
## projet blog avec Silex

*“ Silex est un micro framework PHP s’inspirant de son grand frère : le framework Symfony “*


### LE PROJET BLOG :

Ce projet a pour simple but de récupérer des articles et de les afficher. On pourra par la suite les modifier, c’est-à-dire : soit les éditer (changer le titre, le contenu…) soit les supprimer. Et enfin pouvoir en ajouter autant que l’on souhaite.

### LES OUTILS UTILISÉS :

Pour se faire, j’utilise le micro framework PHP `Silex` qui utilise le gestionnaire de dépendances Composer. `MAMP` pour un serveur local ainsi que `PhpMyAdmin` pour notre base données. Le moteur de template `Twig`. Et pour finir `Postman` qui me permet de vérifier mes requêtes AJAX.



### PRÉREQUIS :

Avant toutes choses, je réalise ma base de données sur PhpMyAdmin avec quelques articles. Ils sont alors composés de la manière suivante :

- Titre
- Auteur
- Date
- Contenu textuel

Pour lier ma base de données avec mon projet, j’ajoute la dépendance `DoctrineServiceProvider` : 
```sh
composer require "doctrine/dbal:~2.2" 
```
### LES DIFFÉRENTES ROUTES :

*Toutes les routes qui suivent sont précédées de `/index_dev.php`*

```sh
“ / “
```
La première route renvoie à la page d’accueil. Cette page comporte un bouton permettant d'accéder à tous les articles du site.
```sh
“ /blog “
```
Cette deuxième route permet de pouvoir accéder à tous les articles de notre site. On peut alors voir les différents articles avec leurs informations : le titre, l’auteur ainsi que la date.
La page comporte également deux boutons : Retour (“/”) et Ajouter un article qui renvoie sur la troisième route…
```sh
“ /blog/slug “
```
Le slug, ici, est une version modifiée du titre de l’article que l’on a choisi mais adapté pour les requêtes GET (dans mon cas j’ai utilisé `Suglify` pour pouvoir changer mon titre “*Titre exemple !*” en “*titre-exemple*”)
La route permet donc d’accéder à l’article, c’est-à-dire : son titre, son auteur, sa date de création et son contenu.

En dessous des informations se trouve trois boutons : un bouton retour (au blog), un bouton éditer et un bouton supprimer.
```sh
“/admin“
```
La route “admin” n’affiche aucune vue mais permet d'amener à d’autres routes comme “edit”, “insert” et “delete”.
```sh
“/admin/insert“
```
La route “insert” est utile à la création d’articles. Sur sa vue : un formulaire avec les informations à remplir (exceptée la date qui se met toute seule à jour).
```sh
“/admin/delete/slug“
```
Cette route n’est accessible que depuis la route `/blog/slug`, elle permet la suppression de l’article de la base de données.
```sh
“/admin/edit/slug“
```
“Edit” permet de modifier notre article avec un formulaire (qui récupère les données actuelles est les affiches dans les “input”).
Pour ce formulaire, les données sont récupérées et envoyées en Ajax grâce à la route suivante :
```sh
“/api/datasingle/slug“
```
Cette route renvoie uniquement un fichier Json de toutes les données des articles pour pouvoir être récupéré par le formulaire de la route précédente.

### RÉALISATION :

Le projet silex est composé de différents dossiers et fichiers, on retrouve alors dans les plus importants :

#### Le dossier src :

Le fichier app.php qui permet de gérer tous les services de l’application et le controller.php qui permet de créer toutes nos routes. Dans ce fichier (controller.php), nous allons beaucoup solliciter la base données :

exemple pour la route `/blog`
```sh
$app->get('/blog', function () use ($app) { //Lorsque l'on fait une requête GET sur /blog
    $sql = "SELECT * FROM articles";      	//On fait une requête sql de tous les articles
    $posts = $app['db']->fetchAll($sql);	//$posts récupère alors toutes les données
    return $app['twig']->render('blog.html.twig', array(
        'posts' => $posts
    )); // On retourne la page blog.html.twig avec les articles
});
```

#### Le dossier templates :

Dans ce dossier template, nous allons retrouver toutes les vues de notre site .html.twig où nous allons mettre en forme toutes les pages. Et de là on récupère les données reçues (en fonction du slug) :
```sh
<p> Titre : {{ post.titre }} </p>
<p> Auteur : {{ post.auteur }} </p>
....
```
#### Le dossier web :

Ce dossier va être notre dossier “public” où l’on retrouve notre `index_dev.php` ainsi que notre fichier main.css

### DIFFICULTÉS :

J’ai pu rencontrer des difficultés lors de la création de ma requête Ajax (dans ma vue `edit.html.twig`)  étant donné que je n’ai fait que très rarement du JQuery. Je me suis alors aidée d’Internet ainsi que de l’aide d’un de mes camarades pour avancer dans mon projet.

Dans l’ensemble, je me suis beaucoup renseignée sur Internet lorsque je rencontrais quelques petits soucis.

### CONCLUSION :

C’est un petit projet qui m’a appris plusieurs choses :

- la gestion des vues avec les controllers (routes)
- la gestion d’une API
- l’utilisation d’un micro framework
- l’utilisation de twig

J’ai bien aimé ce petit exercice/projet qui m’a été et me sera utile.
